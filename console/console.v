module console

import vctru

enum Screen {
	top = 0
	bottom = 1
}

fn C.consoleInit(screen int, console int)

fn C.consoleClear()

pub fn init(screen Screen, console int) {
	C.consoleInit(screen, console)
}

pub fn clear() {
	C.consoleClear()
}
