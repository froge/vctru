module gfx

import vctru

pub enum Screen {
	top
	bottom
}

pub enum Gfx3DSide {
	left
	right
}

fn C.gfxInitDefault()

fn C.gfxExit()

fn C.gfxSet3D(enable bool)

fn C.gfxIs3D() bool

fn C.gfxIsWide() bool

fn C.gfxSetWide(enable bool)

fn C.gfxSetDoubleBuffering(screen Screen, enable bool)

fn C.gfxSwapBuffers()

fn C.gfxFlushBuffers()

fn C.gfxScreenSwapBuffers(screen Screen, has_stereo bool)

pub fn init_default() {
	C.gfxInitDefault()
}

pub fn set_3d(enable bool) {
	C.gfxSet3D(enable)
}

pub fn is_3d() bool {
	return C.gfxIs3D()
}

pub fn is_wide() bool {
	return C.gfxIsWide()
}

pub fn set_wide(enable bool) {
	C.gfxSetWide(enable)
}

pub fn set_double_buffering(screen Screen, enable bool) {
	C.gfxSetDoubleBuffering(screen, enable)
}

pub fn flush_buffers() {
	C.gfxFlushBuffers()
}

pub fn swap_buffers() {
	C.gfxSwapBuffers()
}

pub fn screen_swap_buffers(screen Screen, has_stereo bool) {
	C.gfxScreenSwapBuffers(screen, has_stereo)
}

pub fn gfx_exit() {
	C.gfxExit()
}
