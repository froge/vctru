module hid

import vctru
import math

type Keymask = u32

pub struct TouchPos {
	x u16
	y u16
}

pub struct CirclePos {
	x i16
	y i16
}

pub struct AccelVector {
	x i16
	y i16
	z i16
}

pub struct AngularRate {
	x i16 // Roll
	y i16 // Yaw
	z i16 // Pitch
}

const (
	key_a            = u32(1)
	key_b            = u32(2)
	key_select       = u32(math.pow(2, 2))
	key_start        = u32(math.pow(2, 3))
	key_dright       = u32(math.pow(2, 4))
	key_dleft        = u32(math.pow(2, 5))
	key_dup          = u32(math.pow(2, 6))
	key_ddown        = u32(math.pow(2, 7))
	key_r            = u32(math.pow(2, 8))
	key_l            = u32(math.pow(2, 9))
	key_x            = u32(math.pow(2, 10))
	key_y            = u32(math.pow(2, 11))
	key_zl           = u32(math.pow(2, 14))
	key_zr           = u32(math.pow(2, 15))
	key_touch        = u32(math.pow(2, 20))
	key_cstick_right = u32(math.pow(2, 24))
	key_cstick_left  = u32(math.pow(2, 25))
	key_cstick_up    = u32(math.pow(2, 26))
	key_cstick_down  = u32(math.pow(2, 27))
	key_cpad_right   = u32(math.pow(2, 28))
	key_cpad_left    = u32(math.pow(2, 29))
	key_cpad_up      = u32(math.pow(2, 30))
	key_cpad_down    = u32(math.pow(2, 31))
	key_up           = key_dup | key_cpad_up
	key_down         = key_ddown | key_cpad_down
	key_left         = key_dleft | key_cpad_left
	key_right        = key_dright | key_cpad_right
)

pub enum Event {
	pad0 = 0
	pad1
	accel
	gyro
	debug_pad
	max
}

pub enum Keys {
	a
	b
	_select
	start
	dright
	dleft
	dup
	ddown
	r
	l
	x
	y
	zl
	zr
	touch
	cstick_right
	cstick_left
	cstick_up
	cstick_down
	cpad_right
	cpad_left
	cpad_up
	cpad_down
	up
	down
	left
	right
}

[typedef]
struct C.touchPosition {
	px u16
	py u16
}

[typedef]
struct C.circlePosition {
	dx i16
	dy i16
}

[typedef]
struct C.accelVector {
	x i16
	y i16
	z i16
}

[typedef]
struct C.angularRate {
	x i16
	y i16
	z i16
}

fn C.hidScanInput()

fn C.hidKeysHeld() u32

fn C.hidKeysDown() u32

fn C.hidKeysDownRepeat() u32

fn C.hidKeysUp() u32

fn C.hidSetRepeatParameters(delay u32, interval u32)

fn C.hidTouchRead(&C.touchPosition)

fn C.hidCircleRead(&C.circlePosition)

fn C.hidAccelRead(&C.accelVector)

fn C.hidGyroRead(&C.angularRate)

fn C.hidWaitForEvent(Event, bool)

fn C.hidWaitForAnyEvent(bool, Handle, i64)

fn C.HIDUSER_GetHandles(&Handle, &Handle, &Handle, &Handle, &Handle, &Handle)

fn C.HIDUSER_EnableAccelerometer()

fn C.HIDUSER_DisableAccelerometer()

fn C.HIDUSER_EnableGyroscope()

fn C.HIDUSER_DisableGyroscope()

fn C.HIDUSER_GetGyroscopeRawToDpsCoefficient(&f32)

fn C.HIDUSER_GetSoundVolume(&byte)

fn get_mask(key Keys) u32 {
	ke := match key {
		.a { key_a }
		.b { key_b }
		._select { key_select }
		.start { key_start }
		.dright { key_dright }
		.dleft { key_dleft }
		.dup { key_dup }
		.ddown { key_ddown }
		.r { key_r }
		.l { key_l }
		.x { key_x }
		.y { key_y }
		.zl { key_zl }
		.zr { key_zr }
		.touch { key_touch }
		.cstick_right { key_cstick_right }
		.cstick_left { key_cstick_left }
		.cstick_up { key_cstick_up }
		.cstick_down { key_cstick_down }
		.cpad_right { key_cpad_right }
		.cpad_left { key_cpad_left }
		.cpad_up { key_cpad_up }
		.cpad_down { key_cpad_down }
		.up { key_up }
		.down { key_down }
		.left { key_left }
		.right { key_right }
	}
	return ke
}

pub fn scan_input() {
	C.hidScanInput()
}

// Check if keymask contains a key
pub fn (mask Keymask) contains(keys ...Keys) bool {
	mut ke := u32(0)
	for key in keys {
		ke |= get_mask(key)
	}
	if ke & mask == ke {
		return true
	} else {
		return false
	}
}

// Get a bitmask that represents held keys
pub fn keys_held() Keymask {
	return C.hidKeysHeld()
}

pub fn keys_down() Keymask {
	return C.hidKeysDown()
}

pub fn keys_down_repeat() Keymask {
	return C.hidKeysDownRepeat()
}

pub fn key_up() Keymask {
	return C.hidKeysUp()
}

pub fn set_repeat_parameters(delay u32, interval u32) {
	C.hidSetRepeatParameters(delay, interval)
}

// Read the touch position
pub fn touch_read() TouchPos {
	pos := C.touchPosition{}
	unsafe { C.hidTouchRead(&pos) }
	return {
		x: pos.px
		y: pos.py
	}
}

// Read the circle position
pub fn circle_read() CirclePos {
	pos := C.circlePosition{}
	unsafe { C.hidCircleRead(&pos) }
	return {
		x: pos.dx
		y: pos.dy
	}
}

pub fn accel_read() AccelVector {
	pos := C.accelVector{}
	unsafe { C.hidAccelRead(&pos) }
	return {
		x: pos.x
		y: pos.y
		z: pos.z
	}
}

pub fn gyro_read() AngularRate {
	pos := C.angularRate{}
	unsafe { C.hidGyroRead(&pos) }
	return {
		x: pos.x
		y: pos.y
		z: pos.z
	}
}

pub fn wait_for_event(event Event, discard bool) {
	C.hidWaitForEvent(event, discard)
}

// TODO add get_handles
pub fn enable_accelerometer() {
	C.HIDUSER_EnableAccelerometer()
}

pub fn disable_accelerometer() {
	C.HIDUSER_DisableAccelerometer()
}

pub fn enable_gyroscope() {
	C.HIDUSER_EnableGyroscope()
}

pub fn disable_gyroscope() {
	C.HIDUSER_DisableGyroscope()
}

pub fn get_gyroscope_raw_to_dps_coefficient() f32 {
	coeff := f32(0)
	unsafe { C.HIDUSER_GetGyroscopeRawToDpsCoefficient(&coeff) }
	return coeff
}

pub fn get_sound_volume() byte {
	vol := byte(0)
	unsafe { C.HIDUSER_GetSoundVolume(&vol) }
	return vol
}
