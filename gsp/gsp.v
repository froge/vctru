module gsp

import vctru

fn C.gspWaitForVBlank()

pub fn wait_for_vblank() {
	C.gspWaitForVBlank()
}
