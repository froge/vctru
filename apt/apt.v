module apt

import vctru

enum HookType {
	onsuspend
	onrestore
	onsleep
	onwakeup
	onexit
	count
}

enum NSAppId {
	homemenu = 0x101
	camera = 0x110
	friends_list = 0x112
	game_notes = 0x113
	web = 0x114
	instruction_manual = 0x115
	notifications = 0x116
	miiverse = 0x117
	miiverse_posting = 0x118
	amiibo_settings = 0x119
	application = 0x300
	eshop = 0x301
	software_keyboard = 0x401
	mii_selector = 0x402
	photo_selector = 0x404
	sound_selector = 0x405
	err_disp = 0x406
	mint = 0x407
	circle_pad_pro_calibrator = 0x408
	notepad = 0x409
}

type AptHookFn = fn (HookType, voidptr)

type AptMessageCb = fn (voidptr, NSAppId, voidptr, int)

struct C.aptHookCookie {
	next     &C.aptHookCookie
	callback AptHookFn
	param    voidptr
}

fn C.aptMainLoop() bool

fn C.aptInit()

fn C.aptExit()

fn C.aptSendCommand(&u32)

fn C.aptIsActive() bool

fn C.aptShouldClose() bool

fn C.aptIsSleepAllowed() bool

fn C.aptSetSleepAllowed(allowed bool)

fn C.aptHandleSleep()

fn C.aptIsHomeAllowed() bool

fn C.aptSetHomeAllowed(allowed bool)

fn C.aptShouldJumpToHome() bool

fn C.aptCheckHomePressRejected() bool

fn C.aptJumpToHomeMenu()

fn C.aptHandleJumpToHome()

fn C.aptHook(&C.aptHookCookie, AptHookFn, voidptr)

fn C.aptUnhook(&C.aptHookCookie)

fn C.aptSetMessageCallback(AptMessageCb, voidptr)

fn C.aptLaunchLibraryApplet(NSAppId, byteptr, int, Handle)

fn C.aptClearChainloader()

fn C.aptSetChainloader(program_id u64, mediatype byte)

fn C.aptSetChainloaderToSelf()

fn C.APT_GetLockHandle(u16, &Handle)

// TODO: add various other c functions
fn C.APT_HardwareResetAsync()

fn C.aptGetMenuAppID() NSAppId

fn C.APT_SetAppCpuTimeLimit(u32)

fn C.APT_GetAppCpuTimeLimit(&u32)

fn C.APT_CheckNew3DS(&bool)

fn C.APT_PrepareToDoApplicationJump(byte, u64, byte)

fn C.APT_DoApplicationJump(voidptr, int, voidptr)

fn C.APT_PrepareToStartSystemApplet(NSAppId)

fn C.APT_StartSystemApplet(NSAppId, voidptr, int, Handle)

fn C.APT_GetSharedFont(&Handle, &u32)

// Handles sleep mode, HOME and power buttons, call at beginning of every frame.
// Returns if the application should continue to run
pub fn main_loop() bool {
	return C.aptMainLoop()
}

// Returns if the application is in the foreground
pub fn is_active() bool {
	return C.aptIsActive()
}

// Initializes APT
pub fn init() {
	C.aptInit()
}

// Exits APT
pub fn apt_exit() {
	C.aptExit()
}

pub fn check_home_press_rejected() bool {
	return C.aptCheckHomePressRejected()
}

pub fn launch_lib_applet(appid NSAppId) {
	mut aptbuf := []u32{cap: 255}
	aptbuf.clear()
	C.aptLaunchLibraryApplet(appid, aptbuf.data, 256, 0)
}

pub fn clear_chainloader() {
	C.aptClearChainloader()
}

pub fn set_chainloader(program_id u64, mediatype byte) {
	C.aptSetChainloader(program_id, mediatype)
}

pub fn set_chainloader_to_self() {
	C.aptSetChainloaderToSelf()
}
