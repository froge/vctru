module am

import vctru
import fs

type Handle = u32

[typedef]
struct C.AM_TitleEntry {
	titleID u64
	size    u64
	version u16
}

fn C.amInit()

fn C.amAppInit()

fn C.amExit()

fn C.amGetSessionHandle() Handle

fn C.AM_GetTitleCount(mediatype fs.MediaType, count &u32)

fn C.AM_GetTitleList(titles_read &u32, mediatype fs.MediaType, title_count u32, title_ids byteptr)

fn C.AM_GetTitleInfo(mediatype fs.MediaType, title_count u32, title_ids &u64, title_info &C.AM_TitleEntry)

struct TitleEntry {
	title_id u64
	size     u64
	version  u16
}

pub fn init() {
	C.amInit()
}

pub fn app_init() {
	C.amAppInit()
}

pub fn am_exit() {
	C.amExit()
}

pub fn get_title_list(mediatype fs.MediaType, title_count int) (u32, []u64) {
	titles_read := u32(0)
	title_id_ptr := unsafe { malloc(8 * title_count) }
	unsafe { C.AM_GetTitleList(&titles_read, mediatype, title_count, title_id_ptr) }
	title_ids := unsafe {
		array{
			element_size: 8
			data: title_id_ptr
			len: int(titles_read)
			cap: int(titles_read)
		}
	}
	return titles_read, title_ids
}

pub fn get_title_count(media_type fs.MediaType) u32 {
	count := u32(0)
	unsafe { C.AM_GetTitleCount(media_type, &count) }
	return count
}

pub fn get_title_info(media_type fs.MediaType, title_id u64) TitleEntry {
	entry := C.AM_TitleEntry{}
	C.AM_GetTitleInfo(media_type, 1, &title_id, &entry)
	return TitleEntry{
		title_id: entry.titleID
		size: entry.size
		version: entry.version
	}
}

/*
pub fn get_title_info(media_type fs.MediaType, title_ids []u64) []TitleEntry {
	mut entries := []C.AM_TitleEntry{cap: title_ids.len}
	entry_ptr := C.AM_TitleEntry{}
	mut titles := []TitleEntry{}
	C.AM_GetTitleInfo(media_type, title_ids.len, title_ids.data, &entry_ptr)
	entries.insert_many(0, entry_ptr, title_ids.len)
	for entry in entries {
		titles << TitleEntry{
			title_id: entry.titleID
			size: entry.size
			version: entry.version
		}
	}
	return titles
}
*/
